use std::env;

use reqwest::{header, Client};
use serde::{Deserialize, Serialize};

#[derive(Clone)]
pub struct GiteaClient {
    client: Client,
    instance_url: String,
}

impl GiteaClient {
    pub fn new(token: &str, instance_url: &str) -> GiteaClient {
        let mut headers = header::HeaderMap::new();
        headers.insert(
            header::AUTHORIZATION,
            header::HeaderValue::from_bytes(format!("token {}", token).as_bytes()).unwrap(),
        );

        let instance_url = instance_url.trim_end_matches('/');
        let instance_url = format!("{}/api/v1", instance_url);

        GiteaClient {
            client: Client::builder()
                .user_agent(format!("gitea-stale-bot v{}", env!("CARGO_PKG_VERSION")))
                .default_headers(headers)
                .build()
                .expect("failed to build https client"),
            instance_url,
        }
    }

    pub async fn get_repos(&self, page: u32) -> Vec<Repository> {
        self.client
            .get(format!("{}/user/repos?page={}", self.instance_url, page))
            .send()
            .await
            .unwrap()
            .json()
            .await
            .unwrap()
    }

    pub async fn get_issues(&self, repository: &Repository, page: u32) -> Vec<Issue> {
        self.client
            .get(format!(
                "{}/repos/{}/issues?state=open&type=issues&page={}",
                self.instance_url, repository.full_name, page
            ))
            .send()
            .await
            .unwrap()
            .json()
            .await
            .unwrap()
    }

    pub async fn get_labels(&self, repository: &Repository, page: u32) -> Vec<Label> {
        self.client
            .get(format!(
                "{}/repos/{}/labels?page={}",
                self.instance_url, repository.full_name, page
            ))
            .send()
            .await
            .unwrap()
            .json()
            .await
            .unwrap()
    }

    pub async fn add_labels(&self, repository: &Repository, issue: &Issue, labels: Vec<i64>) {
        #[derive(Serialize)]
        struct LabelBody {
            labels: Vec<i64>,
        }

        self.client
            .post(format!(
                "{}/repos/{}/issues/{}/labels",
                self.instance_url, repository.full_name, issue.number
            ))
            .json(&LabelBody { labels })
            .send()
            .await
            .unwrap();
    }

    pub async fn close_issue(&self, repository: &Repository, issue: &Issue) {
        #[derive(Serialize)]
        struct IssueBody {
            state: &'static str,
        }

        static BODY: IssueBody = IssueBody { state: "closed" };

        self.client
            .patch(format!(
                "{}/repos/{}/issues/{}",
                self.instance_url, repository.full_name, issue.number
            ))
            .json(&BODY)
            .send()
            .await
            .unwrap();
    }
}

// JSON definitions

#[derive(Debug, Deserialize)]
pub struct Repository {
    pub name: String,
    pub owner: User,
    pub full_name: String,
    pub has_issues: bool,
    pub has_pull_requests: bool,
    pub permissions: Permissions,
}

#[derive(Debug, Deserialize)]
pub struct User {
    pub username: String,
}

#[derive(Debug, Deserialize)]
pub struct Permissions {
    pub admin: bool,
    pub push: bool,
    pub pull: bool,
}

#[derive(Debug, Deserialize)]
pub struct Issue {
    pub url: String,
    pub number: i64,
    pub updated_at: chrono::DateTime<chrono::Utc>,
    pub labels: Vec<Label>,
}

#[derive(Debug, Deserialize)]
pub struct Label {
    pub id: i64,
    pub name: String,
}
