use crate::{
    error::{Error, Result},
    gitea_client::{GiteaClient, Repository},
};

pub struct RepoHandler {
    client: GiteaClient,
    repository: Repository,
    minutes_to_stale: i64,
    minutes_to_close: i64,
    stale_label_id: i64,
}

impl RepoHandler {
    pub fn new(client: GiteaClient, repository: Repository) -> RepoHandler {
        RepoHandler {
            client,
            repository,
            minutes_to_stale: 5,
            minutes_to_close: 2,
            stale_label_id: 0,
        }
    }

    pub async fn handle_repository(mut self) {
        if let Err(err) = self.get_label_id().await {
            match err {
                Error::NoStaleLabel => eprintln!("no stale label in repository '{}'", self.repository.full_name),
                _ => eprintln!("{:?}", err),
            }
            return;
        }
        self.mark_and_close_issues().await;
    }

    async fn mark_and_close_issues(&mut self) {
        let time_now = chrono::Utc::now();

        let mut page = 1;
        loop {
            let issues = self.client.get_issues(&self.repository, page).await;

            if issues.len() == 0 {
                break;
            }

            for issue in issues.iter() {
                let time_diff = time_now - issue.updated_at;
                // check if issue is already marked as stale
                if issue.labels.iter().any(|l| l.id == self.stale_label_id) {
                    if time_diff.num_minutes() > self.minutes_to_close {
                        self.client.close_issue(&self.repository, &issue).await;
                    }
                } else {
                    if time_diff.num_minutes() > self.minutes_to_stale {
                        self.client
                            .add_labels(&self.repository, &issue, vec![self.stale_label_id])
                            .await;
                    }
                }
            }

            page += 1;
        }
    }

    async fn get_label_id(&mut self) -> Result<()> {
        let mut page = 1;
        loop {
            let labels = self.client.get_labels(&self.repository, page).await;

            if labels.len() == 0 {
                break;
            }

            for label in labels {
                if label.name == "stale" {
                    self.stale_label_id = label.id;
                    return Ok(());
                }
            }

            page += 1;
        }

        Err(Error::NoStaleLabel)
    }
}
