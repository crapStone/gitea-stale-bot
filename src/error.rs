use thiserror::Error;

#[derive(Error, Debug)]
#[non_exhaustive]
pub enum Error {
    #[error("stale label not set")]
    NoStaleLabel,
}

pub type Result<T> = std::result::Result<T, Error>;
