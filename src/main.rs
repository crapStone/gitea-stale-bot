mod error;
mod gitea_client;
mod repo_handler;

use std::env;

use futures::future;
use tokio::task;

use crate::{gitea_client::GiteaClient, repo_handler::RepoHandler};

#[tokio::main]
async fn main() {
    let token = env::var("GITEA_TOKEN").expect("failed to get GITEA_TOKEN from environment");
    let client = GiteaClient::new(&token, "http://localhost:3000".into());

    let mut page = 1;

    loop {
        let repos = client.get_repos(page).await;

        let mut handles = vec![];

        if repos.len() == 0 {
            break;
        }

        for repo in repos {
            let handler = RepoHandler::new(client.clone(), repo);
            handles.push(task::spawn(handler.handle_repository()));
        }

        future::join_all(handles).await;

        page += 1;
    }
}
